<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    // public function rules(): array
    // {
    //     return [
    //         'name' => 'required',
    //         'phone' => 'required|unique:users,phone,'.$this->user,
    //         'gender' => 'required',
    //         'image' => 'nullable|image|mimes:png,jpg,PNG,JPEG',
    //         'password' => 'nullable|min:6',
    //         'email' => 'required|email|unique:users,email,'.$this->user,
    //     ];
    // }
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'gender' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,PNG,JPEG',
            'password' => 'nullable|min:6',
        ];

        // Kiểm tra xem email và phone có được sửa đổi không
        if ($this->user && $this->isEmailChanged()) {
            $rules['email'] = 'required|email|unique:users,email';
        }

        if ($this->user && $this->isPhoneChanged()) {
            $rules['phone'] = 'required|unique:users,phone';
        }

        return $rules;
    }

}
