<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = $this->product->latest('id')->paginate(12);
        return view('client.home.index', compact('products'));
    }   
    
    public function search(Request $request)
    {
        $searchQuery = $request->input('name');
    
        $products = Product::where('name', 'like', '%' . $searchQuery . '%')->paginate(12);

        
        if ($products->isEmpty()) {
            //return view('client.products.index', compact('products'))->with(['message' => 'Không tìm thấy sản phẩm có tên ' . $searchQuery]);
            session()->flash('message', 'Không tìm thấy sản phẩm có tên: ' .'`'. $searchQuery.'`');
        // Kiểm tra xem session 'message' đã được thiết lập hay chưa
           // dd(session()->get('message'));
            return view('client.home.index', compact('products'));
        } else {
            return view('client.home.index', compact('products'));
        }
    }
}
