<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UpdateUserRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{   
    protected $user;
    public function __construct(User $user){
        $this->user = $user; 

    }
    public function showAccount(){
        $user = $this->user ->findOrFail(auth()->user()->id);
        $roles = $user->roles;
        //dd($role);
        return view('client.user.index',compact('user','roles'));
    
    }
    public function viewUpdateProfile(){
        $user = User::findOrFail(auth()->user()->id);
        return view('client.user.edit',compact('user'));
    }
    public function UpdateProfile(UpdateUserRequest $request){
        $dataUpdate = $request->except('password');
        $user = $this->user->findOrFail(auth()->user()->id);
        if ($request->password) {
            $dataUpdate['password']= Hash::make($request->password);
        }
        $currentImage = $user->images ? ($user->images->first() ? $user->images->first()->url : asset('upload/default.png')) :asset('upload/default.png');

        $dataUpdate['image'] = $this->user->updateImage($request, $currentImage);
       
        $user->update($dataUpdate);
        $user->images()->delete();
        $user->images()->Create(['url' => $dataUpdate['image']]);
        //dd($dataUpdate);
        return redirect()->route('client.home.account')->with(['message'=>'Update profile success']);
    }
}
