<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\CreateOrderRequest;
use App\Http\Resources\Cart\CartResource;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{

    protected $cart;
    protected $product;
    protected $cartProduct;
    protected $coupon;
    protected $order;

    public function __construct(Product $product, Cart $cart, CartProduct $cartProduct, Coupon $coupon, Order $order)
    {

        $this->product = $product;
        $this->cart = $cart;
        $this->cartProduct = $cartProduct;
        $this->coupon = $coupon;
        $this->order = $order;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cart=$this->cart->firstOrCreateBy(auth()->user()->id)->load('products');
        return view('client.carts.index', compact('cart'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->product_size) {
            $product = $this->product->FindOrFail($request->product_id);
            // $a =auth()->user()->id;
            // dd($a);
            
            $cart = $this->cart->firstOrCreateBy(auth()->user()->id);
            
            $cartProduct = $this->cartProduct->getBy($cart->id, $product->id, $request->product_size);
            //dd($request->product_size);
            // $a =$cart->id;
            // dd($a);
            //dd($cartProduct);
            if ($cartProduct) {
                $quantity = $cartProduct->product_quantity;
                $cartProduct->update(['product_quantity'=>($quantity + $request->product_quantity)]);
            } else {
                $dataCreate['cart_id']=$cart->id;
                $dataCreate['product_size']=$request->product_size;
                $dataCreate['product_quantity']=$request->product_quantity ?? 1;
                $dataCreate['product_price']=$product->price;
                $dataCreate['product_id']=$request->product_id;
                
                $this->cartProduct->create($dataCreate);
            }
            return back()->with(['message' => 'Thêm mới thành công']);
        } else {
            return back()->with(['message' => 'Bạn chưa chọn size']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    public function updateQuantityProduct(Request $request, string $id)
    {
        $cartProduct = $this->cartProduct->find($id);
        $dataUpdate = $request->all();

        //dd($dataUpdate,$cartProduct);
        if ($dataUpdate['product_quantity'] < 1) {
            $cartProduct->delete();
        } else {
            $cartProduct->update($dataUpdate);
        }
        $cart = $cartProduct->cart;

        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart),
            'remove_product' => $dataUpdate['product_quantity'] < 1,
            'cart_product_price'=> $cartProduct->total_price
        ], Response::HTTP_OK);
    }
    public function removeProductInCart(string $id)
    {
         $cartProduct =  $this->cartProduct->find($id);
         $cartProduct->delete();
         $cart =  $cartProduct->cart;
         return response()->json([
             'product_cart_id' => $id,
             'cart' => new CartResource($cart)
         ], Response::HTTP_OK);
    }

    public function applyCoupon(Request $request)
    {

        $name = $request->input('coupon_code');
        $coupon = $this->coupon->firstWithExperyDate($name, auth()->user()->id);
        if ($coupon) {
            $message="Áp mã giảm giá thành công !";
            Session::put('coupon_id', $coupon->name);
            Session::put('discount_amount_price', $coupon->value);
            Session::put('coupon_code', $coupon->name);
        } else {
            Session::forget(['coupon_id','discount_amount_price','coupon_code']);
            $message="Mã giảm giá không tồn tại hoặc hết hạn !";
        }
        return redirect()->route('client.cart.index')->with([
            'message' => $message,
        ]);
    }

    public function checkout()
    {

        $cart = $this->cart->firstOrCreateBy(auth()->user()->id)->load('products');
        return view('client.carts.checkout', compact('cart'));
    }
  
    // public function processCheckout(CreateOrderRequest $request)
    // {

    //     $dataCreate = $request->all();
    //     $dataCreate['user_id'] = auth()->user()->id;
    //     $dataCreate['status'] = 'pending';

    //     $this->order->create($dataCreate);

    //     $couponID = Session::get('coupon_id');
    //     if ($couponID) {
    //         $coupon = $this->coupon->find(Session::get('coupon_id'));
    //         if ($coupon) {
    //             $coupon->users()->attach(auth()->user()->id, ['value' => $coupon->value]);
    //         }
    //     }
    //     $cart = $this->cart->firstOrCreateBy(auth()->user()->id);
    //     $cart->products()->delete();
    //     Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);
    //     return redirect()->route('client.cart.index')->with(['message'=>'Order success']);
    // }
    public function processCheckout(CreateOrderRequest $request)
    {
        $dataCreate = $request->all();
        $dataCreate['user_id'] = auth()->user()->id;
        $dataCreate['status'] = 'pending';

        // Tạo đơn hàng và lấy nó
        $order = $this->order->create($dataCreate);

        // Lấy mã giảm giá từ session
        $couponID = Session::get('coupon_id');

        if ($couponID) {
            // Tìm mã giảm giá
            $coupon = $this->coupon->find($couponID);

            if ($coupon) {
                // Tạo kết nối giữa người dùng và mã giảm giá
                $coupon->users()->attach(auth()->user()->id, ['value' => $coupon->value]);
            }
        }

        // Lấy giỏ hàng của người dùng
        $cart = $this->cart->firstOrCreateBy(auth()->user()->id);

        // Lấy danh sách sản phẩm trong giỏ hàng
        $cartProducts = $cart->products;

        // Thêm sản phẩm vào đơn hàng và bảng trung gian product_orders
        foreach ($cartProducts as $cartProduct) {
            $product = $cartProduct->product;
            $order->products()->attach($product, [
                'product_size' => $cartProduct->product_size,
                'product_quantity' => $cartProduct->product_quantity,
                'product_price' => $cartProduct->product_price,
            ]);
        }

        // Xoá sản phẩm khỏi giỏ hàng
        $cart->products()->delete();

        // Xoá thông tin mã giảm giá từ session
        Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);

        return redirect()->route('client.cart.index')->with(['message' => 'Order success']);
    }

}
