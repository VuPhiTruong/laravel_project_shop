<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\CreateProductRequest;
use App\Http\Requests\Products\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{


    protected $category;
    protected $product;
    protected $productDetail;

    public function __construct(Product $product, Category $category, ProductDetail $productDetail)
    {
        $this->product = $product;
        $this->category = $category;
        $this->productDetail = $productDetail;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = $this->product::latest('id')->paginate(5);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = $this->category::get(['id','name']);
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateProductRequest $request)
    {
        //DB::enableQueryLog();
        $dataCreate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $product = Product::create($dataCreate);
        $dataCreate['image'] = $this->product->saveImage($request);
        $product->images()->create(['url' => $dataCreate['image']]);
        $product->assignCategory($dataCreate['category_ids']);

        // Lưu dữ liệu sizes vào bảng product_details
        $sizeArray = [];
        foreach ($sizes as $size) {
            $sizeArray[] = [
                'size' => $size->size,
                'quantity' => $size->quantity,
                'product_id' => $product->id,
                
            ];
        }

        $this->productDetail->insert($sizeArray);
        //$queries = DB::getQueryLog();
        //dd($dataCreate);
        //dd($queries);
        return redirect()->route('products.index')->with(['message' => 'Create product '.$product->name.' success']);
    }


    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = $this->product->with(['details','categories'])->findOrFail($id);
        
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $product = $this->product->with(['details','categories'])->findOrFail($id);
        $categories = $this->category->get(['id','name']);
        return view('admin.products.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $dataUpdate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
            
        $product = $this->product->findOrFail($id);
        $currentImage = $product->images ? $product->images->first()->url : '';
        $dataUpdate['image'] = $this->product->updateImage($request, $currentImage);

        $product->update($dataUpdate);
        $product->images()->delete();
        $product->images()->create(['url' => $dataUpdate['image']]);
        $product->assignCategory($dataUpdate['category_ids']);

        // Lưu dữ liệu sizes vào bảng product_details
        $sizeArray = [];
        foreach ($sizes as $size) {
            $sizeArray[] = [
                'size' => $size->size,
                'quantity' => $size->quantity,
                'product_id' => $product->id,
            ];
        }
        $product->details()->delete();
        $this->productDetail->insert($sizeArray);
        //dd($dataCreate);
        return redirect()->route('products.index')->with(['message' => 'Update product '.$product->name.' success']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $product->details()->delete();
        $product->images()->delete();
        $imageName = $product->images-> count() > 0 ? $product->images->first()->url : '';
        $this->product->deleteImage($imageName);
        $product->delete();
        return redirect()->route('products.index')->with(['message' => 'Delete product '.$product->name.' success']);
    }
}
