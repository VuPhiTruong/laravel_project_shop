<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\Config;
use Barryvdh\DomPDF\Facade\PDF;
//use PDF;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Http\Response;



class OrderController extends Controller
{

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function index()
    {
        $orders = $this->order::latest('id')->paginate(10);
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     */
    
    public function updateStatus(Request $request, string $id)
    {
        $order = $this->order->findOrFail($id);
        $order->update(['status' => $request->status]);
        return response()->json([
            'message' => 'success'
        ], Response::HTTP_OK);
    }

    // public function print($id)
    // {
    //     $order = Order::find($id);
    
    //     if (!$order) {
    //         return redirect()->back()->with(['message' => 'Không tìm thấy đơn hàng']);
    //     }
        
        
    //     $products = $order->products;
        
        
    //     $pdf = PDF::loadView('admin.orders.print', compact('order', 'products'));

    //     return $pdf->stream('HoaDon.pdf');
    // }

    public function print($id)
    {
        $order = Order::find($id);
        //dd($order);
        if (!$order) {
            return redirect()->back()->with(['message' => 'Không tìm thấy đơn hàng']);
        }

        $products = $order->products;
        
        $pdf = PDF::loadView('admin.orders.print', compact('order', 'products'))
            ->setOption('autoScriptToLang', true)
            ->setOption('autoLangToFont', true)
            // ->setOption('isHtml5ParserEnabled', true)
            // ->setOption('isPhpEnabled', true)
            // ->setOption('defaultFont', 'Times New Roman')
            ;
                     
        return $pdf->stream('HoaDon.pdf');
    }
}// $fontPath = Config::get('pdf.font_path');
        // $font = Config::get('pdf.font');   
// ->setOption(['font_path' => $fontPath])
            // ->setOption(['font' => $font]);  