<?php

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserCanNotLoginDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
         
        if (Gate::allows('show-dashboard')) {
            return $next($request);
        }
        abort(403, 'Không có quyền truy cập.');

        //return $next($request);
    }
}
