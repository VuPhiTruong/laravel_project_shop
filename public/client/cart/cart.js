$(function() {


    // Truy xuất CSRF token từ meta tag
    const csrfToken = $('meta[name="csrf-token"]').attr('content');
    getTotalValue()

    function getTotalValue() {
        let total = parseFloat($('.total-price').data('price')); // Parse the price as a float

        let couponPriceElement = $('.coupon-div');
        let couponPrice = couponPriceElement.length ? parseFloat(couponPriceElement.data('price')) : 0;

        let totalPriceAfterCoupon = total - couponPrice;

        $('.total-price-all').text(`${totalPriceAfterCoupon.toFixed(2)}VNĐ`); // Format the total price to display two decimal places
    }


    $(document).on("click", ".btn-remove-product", function(e) {
        let url = $(this).data("action");

        confirmDelete()
            .then(function() {
                $.post(
                    url, {
                        _token: csrfToken, // Thêm CSRF token vào dữ liệu gửi đi
                    },
                    (res) => {
                        let cart = res.cart;
                        let cartProductId = res.product_cart_id;
                        $("#productCountCart").text(cart.product_count);
                        $(".total-price")
                            .text(`${cart.total_price}VNĐ`)
                            //.data("price", cart.product_count);
                            .data("price", cart.total_price);
                        $(`#row-${cartProductId}`).remove();
                        getTotalValue()
                    }
                );
            })
            .catch(function() {});
    });


    const TIME_TO_UPDATE = 1000;
    $(document).on('click', '.btn-update-quantity', _.debounce(function(e) {
        // Ngăn chặn hành vi mặc định của nút
        e.preventDefault();

        // Lấy thông tin cần thiết
        let clickedButton = $(this);
        let url = clickedButton.data('action');
        let id = clickedButton.data('id');
        let inputField = $(`#productQuantityInput-${id}`);
        let currentQuantity = parseInt(inputField.val());
        let newQuantity = clickedButton.hasClass('btn-plus') ? currentQuantity + 1 : currentQuantity - 1;

        // Cập nhật số lượng sản phẩm trên giao diện
        inputField.val(newQuantity);

        // Dữ liệu gửi đến máy chủ
        let data = {
            _token: csrfToken,
            product_quantity: newQuantity,
        };

        // Gửi yêu cầu AJAX đến máy chủ
        $.post(url, data, (res) => {
            let cartProductId = res.product_cart_id;
            let cart = res.cart;
            $('#productCountCart').text(cart.product_count)
            if (res.remove_product) {
                $(`#row-${cartProductId}`).remove();
            } else {
                $(`#cartProductPrice${cartProductId}`).html(`${res.cart_product_price}VNĐ`);
            }

            //
            $('.total-price').text(`${cart.total_price} VNĐ`)
                .data("price", cart.total_price);
            getTotalValue()
                //cartProductPrice
            Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Success",
                showConfirmButton: false,
                timer: 1500,
            });
        });
    }, TIME_TO_UPDATE));
});