@extends('admin.layouts.app')
@section('title','Order')
@section('content')
<div class="card">

    @if (session('message'))
        <h3 class="text-primary">
            {{session('message')}}
        </h3>
    @endif

    <h1>
        Order list
    </h1>       
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Status</th>
                <th>Total</th>
                <th>Ship</th>
                <th>Customer name</th>
                <th>Customer email</th>
                <th>Customer phone</th>
                <th>Customer address</th>
                <th>Note</th>
                <th>Payment</th>
            </tr>
            @foreach ($orders as $item)
                <tr>                   
                    <td>{{$item->id}}</td>
                    <td>
                        <div class="input-group input-group-static mb-4">
                            <select name="status" class="form-control select-status"
                            data-action="{{route('admin.orders.update_status',$item->id)}}">
                            @foreach (config('order.status') as $status)
                                <option value="{{$status}}"
                                       {{$status == $item->status ? 'selected' : ''}}>{{$status}}
                                </option>                               
                            @endforeach
                            </select>
                        </div>
                    </td>
                    <td>{{$item->total}} VNĐ</td>
                    <td>{{$item->ship}} VNĐ</td>
                    <td>{{$item->customer_name}}</td>
                    <td>{{$item->customer_email}}</td>
                    <td>{{$item->customer_phone}}</td>
                    <td>{{$item->customer_address}}</td>
                    <td>{{$item->note}}</td>
                    <td>{{$item->payment}}</td>   
                    <td>
                        <!-- Nút In hóa đơn  -->
                        <a href="{{ route('admin.orders.print', $item->id) }}" class="btn btn-info">Print</a>
                    </td>                 
                </tr>
            @endforeach
        </table>
        {{ $orders->links()}}
    </div>

</div>
    
@endsection
@section('script')
    <script src="{{asset('admin/assets/order/order.js')}}"></script>
@endsection