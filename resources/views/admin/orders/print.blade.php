<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <style>
        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
        }

        h1 {
            font-size: 24px;
            text-align: center;
        }

        h2 {
            font-size: 20px;
            margin-top: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 10px;
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }

        p {
            font-size: 16px;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <h1>Invoice</h1>      
    <p>Name: {{ $order->customer_name }}</p>
    <p>Email: {{ $order->customer_email }}</p>
    <p>Phone: {{ $order->customer_phone }}</p>
    <p>Address: {{ $order->customer_address }}</p>
    <p>Note: {{ $order->note }}</p>
    
    <h2>Detail</h2>
    <table>
        <thead>
            <tr>
                <th>Product</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>SubTotal</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->pivot->product_size }}</td>
                    <td>{{ $product->pivot->product_quantity }}</td>
                    <td>{{ number_format($product->price, 0, ',', '.') }} VND</td>
                    <td>{{ number_format($product->pivot->product_quantity * $product->price, 0, ',', '.') }} VND</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <p>Payment: {{ $order->payment }}</p>
    <p>Ship: 50.000 VND</p>
    <p style="font-size: 20px; font-weight: bold; float: right;">Total: {{ number_format($order->total, 0, ',', '.') }} VND</p>
   
    
</body>
</html>
