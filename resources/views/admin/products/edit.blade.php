@extends('admin.layouts.app')
@section('title','Edit Product')
@section('content')
    <div class="card"> 
        <h1>Edit Product</h1>

        <div>
            <form action="{{ route('products.update',$product->id)}}" id="createForm" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="row">                   
                    <div class="input-group-static col-5 mb-4">
                        <label>Image</label>
                        <input type="file" accept="image/*" name="image" id="image-input" class="form-control">                           
                        @error('image')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <img src="{{ $product->images ? asset('upload/'.$product->images->first()->url) : 'upload/product_default.jpg'}}"
                         id="show-image" alt="" width="300px" >
                    </div>                    
                </div>
                
                
                <div class="input-group input-group-static mb-4">
                    <label for="">Name</label>
                    <input type="text" value="{{ old('name') ?? $product->name}}" name="name" class="form-control">

                    @error('name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>


                <div class="input-group input-group-static mb-4">
                    <label for="">Price</label>
                    <input type="number" step="0.1" value="{{ old('price') ?? $product->price}}" name="price" class="form-control">

                    @error('price')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static mb-4">
                    <label for="">Sale</label>
                    <input type="number" value="0" value="{{ old('sale') ?? $product->sale}}" name="sale" class="form-control">

                    @error('sale')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label for="">Description</label>
                    <div class="row w-100 h-100">
                        <textarea name="description" id="editor" class="form-control"
                         style="width: 100%">{{ old('description') ?? $product->description }}</textarea>
                    
                    </div>               
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

            </div>
            <input type="hidden" id="inputSize" name="size">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#AddSizeModal" style="width: 100px">
                Add size
            </button>
            
            <div class="modal fade" id="AddSizeModal" tabindex="-1" aria-labelledby="AddSizeModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="AddSizeModalLabel">Add size</h5>
                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal"
                            aria-label="Close" >X</button>
                        </div>
                        <div class="modal-body" id="AddSizeModalBody">

                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn  btn-primary btn-add-size"style="margin-left: 10px">Add</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="input-group input-group-static mb-4">
                <label name="group" class="ms-0">Category</label>                   
                <select name="category_ids[]" class="form-control" multiple>
                    @foreach ($categories as $item)
                        <option value="{{$item->id}}"
                            {{$product->categories->contains('id',$item->id) ? 'selected' : ''}}>
                            {{$item->name}}</option>
                    @endforeach    
                </select>

                @error('category_ids')
                    <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <button type="submit" class="btn btn-submit btn-primary" style="width: 100px">Submit</button>
        </form>
    </div>
</div>
@endsection

@section('style')
<style>
    .w-40 {
        width: 40%;
    }

    .w-20 {
        width: 20%;
    }

    .row {
        justify-content: center;
        align-items: center
    }

    .ck.ck-editor {
        width: 100%;
        height: 100%;
    }
    
</style>
@endsection

@section('script')   
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"
        integrity="sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
</script>
<script src="{{ asset('ckeditor5/ckeditor.js') }}"></script>
<script>
    let sizes = @json($product->details)
</script>
<script src="{{ asset('admin/assets/js/product/product.js') }}"></script>
@endsection