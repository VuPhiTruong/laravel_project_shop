@extends('admin.layouts.app')
@section('title','Show Product')
@section('content')
    <div class="card"> 
        <h1>Show Product</h1>

        <div>
            <div class="row">                   
                <div >
                    <label >Image</label>                                          
                </div>
                <div>
                    <img src="{{ $product->images ? asset('upload/'.$product->images->first()->url) : 'upload/product_default.jpg'}}"
                     id="show-image" alt="" width="300px" >
                </div>                    
            </div>
            

            <div>
                <label>Name : {{ $product->name}}</label>
            </div>
            <div>
                <label>Price : {{ $product->price}}</label>
            </div>
            <div>
                <label>Sale : {{ $product->sale}}</label>
            </div>  

            <div class="form-group">      
                <label>Description: 
                    <div>
                        {!! $product->description !!}
                    </div>
                </label>              
                {{--   --}}
                                              
            </div>
            <div>
                <label>Size: 
                    @if ($product->details->count() > 0)                                    
                    @foreach ($product->details as $detail)
                        <p>Size : {{ $detail->size }} - quantity: {{ $detail->quantity }}</p>
                    @endforeach
                    @else
                        <p> Sản phẩm này chưa có size </p>
                    @endif
                </label>
                
            </div>           
            <div>
                <label>Category:
                    @foreach ($product->categories as $item)
                        <p>{{ $item->name }}</p>
                    @endforeach
                </label>
                
            </div>          
        </div>
    </div>
@endsection