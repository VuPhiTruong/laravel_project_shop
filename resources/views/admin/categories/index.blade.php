@extends('admin.layouts.app')
@section('title','Category')
@section('content')
<div class="card">

    @if (session('message'))
        <h3 class="text-primary">
            {{session('message')}}
        </h3>
    @endif

    <h1>
        Category list
    </h1>
    @can('create-category')
        <div>
            <a href="{{ route('categories.create')}}" class="btn btn-primary">Create</a>
        </div>
    @endcan
        
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Parent Name</th>
                <th>Action</th>
            </tr>
            @foreach ($categories as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->parent_name}}</td>
                    <td>
                        @can('update-category')
                            <a href="{{ route('categories.edit', $item->id)}}" class="btn btn-warning">Edit</a>
                        @endcan
                        @can('delete-category')
                            <form action="{{ route('categories.destroy', $item->id)}}" id="form-delete{{$item->id}}"
                                method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn btn-delete btn-danger" type="submit"  data-id={{$item->id}}  >Delete</button>
                            </form>
                        @endcan
                        
                           
                                                  
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $categories->links()}}
    </div>

</div>
    
@endsection
@section('script')   
@endsection