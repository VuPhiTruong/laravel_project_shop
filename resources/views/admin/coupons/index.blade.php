@extends('admin.layouts.app')
@section('title','Coupon')
@section('content')
<div class="card">

    @if (session('message'))
        <h3 class="text-primary">
            {{session('message')}}
        </h3>
    @endif

    <h1>
        Coupon list
    </h1>
        @can('create-coupon')
            <div>
                <a href="{{ route('coupons.create')}}" class="btn btn-primary">Create</a>
            </div>
        @endcan
        
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Value</th>
                <th>Expery date</th>
                <th>Action</th>
            </tr>
            @foreach ($coupons as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->type}}</td>
                    <td>{{$item->value}}</td>
                    <td>{{$item->expery_date}}</td>
                    <td>
                        @can('update-coupon')
                            <a href="{{ route('coupons.edit', $item->id)}}" class="btn btn-warning">Edit</a>
                        @endcan
                        @can('delete-coupon')
                            <form action="{{ route('coupons.destroy', $item->id)}}" id="form-delete{{$item->id}}"
                                method="post">
                            @csrf
                            @method('delete')
                                <button class="btn btn btn-delete btn-danger" type="submit"  data-id={{$item->id}}  >Delete</button>
                            </form> 
                        @endcan
                        
                          
                                                  
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $coupons->links()}}
    </div>

</div>
    
@endsection
@section('script')   
@endsection