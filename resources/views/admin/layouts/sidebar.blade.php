<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="" target="_blank">
        <img src="{{asset('admin/assets/img/logo-ct.png')}}" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Hello {{auth()->user()->name}}</span>
      </a>
    </div>
    
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white {{ request()-> routeIs('dashboard')? 'bg-gradient-primary active':''}}" href="{{route('dashboard')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        @hasrole('super-admin')
          <li class="nav-item">
          <a class="nav-link text-white {{ request()-> routeIs('roles.*')? 'bg-gradient-primary active':''}} "
             href="{{route('roles.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">table_view</i>
            </div>
            <span class="nav-link-text ms-1">Roles</span>
          </a>
        </li>
        @endhasrole       
        @can('show-user')
          <li class="nav-item">
          <a class="nav-link text-white {{ request()->routeIs('users.*') ? 'bg-gradient-primary active' : '' }}"
             href="{{ route('users.index') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person"
                viewBox="0 0 16 16">
                <path
                  d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 2c2.67 0 8 1.33 8 4v1H0v-1c0-2.67 5.33-4 8-4zm0 2a6 6 0 0 0-6 6v1h12v-1a6 6 0 0 0-6-6z" />
              </svg>
            </div>
            <span class="nav-link-text ms-1">Users</span>
          </a>
        </li>
        @endcan
        
        @can('show-product')
          <li class="nav-item">
          <a class="nav-link text-white {{ request()-> routeIs('products.*')? 'bg-gradient-primary active':''}} "
             href="{{route('products.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">view_in_ar</i>
            </div>
            <span class="nav-link-text ms-1">Product</span>
          </a>
        </li>
        @endcan
        
        
        @can('show-category')
          <li class="nav-item">
          <a class="nav-link text-white {{ request()-> routeIs('categories.*')? 'bg-gradient-primary active':''}}"
            href="{{route('categories.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">format_textdirection_r_to_l</i>
            </div>
            <span class="nav-link-text ms-1">Category</span>
          </a>
        </li>
        @endcan
        
        @can('show-coupon')
          <li class="nav-item">
          <a class="nav-link text-white {{ request()->routeIs('coupons.*') ? 'bg-gradient-primary active' : '' }}"
            href="{{ route('coupons.index') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-tag"
                viewBox="0 0 16 16">
                <path
                  d="M13.539 2.225a1 1 0 0 0-1.414 0l-10 10a1 1 0 0 0 1.414 1.414l10-10a1 1 0 0 0 0-1.414z" />
                <path
                  d="M4.293 7.293a1 1 0 0 0 0 1.414L10.586 15a1 1 0 0 0 1.414-1.414L5.707 7.293a1 1 0 0 0-1.414 0z" />
              </svg>
            </div>
            <span class="nav-link-text ms-1">Coupon</span>
          </a>
        </li>
        @endcan
        
        @can('list-order')
          <li class="nav-item">
            <a class="nav-link text-white {{ request()->routeIs('admin.orders.*') ? 'bg-gradient-primary active' : '' }}"
                href="{{ route('admin.orders.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="fa fa-shopping-cart fa-2x"></i> <!-- Sử dụng biểu tượng "Order" từ Font Awesome -->
                </div>
                <span class="nav-link-text ms-1">Order</span>
            </a>
          </li>
        @endcan     
      </ul>
    </div>
    
  </aside>