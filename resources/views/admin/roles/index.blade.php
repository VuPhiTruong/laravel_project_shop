@extends('admin.layouts.app')
@section('title','Roles')
@section('content')
<div class="card">

    @if (session('message'))
        <div class="row">
            <h3 class="text-danger">{{ session('message') }}</h3>
        </div>
    @endif

    <h1>
        Role list
    </h1>
        
            @hasrole('super-admin')
                <div>   
                    <a href="{{ route('roles.create')}}" class="btn btn-primary">Create</a>
                </div>
            @endhasrole
            
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>DisplayName</th>
                <th>Action</th>
            </tr>
            @foreach ($roles as $role)
                <tr>
                    <td>{{$role->id}}</td>
                    <td>{{$role->name}}</td>
                    <td>{{$role->display_name}}</td>
                    <td>
                        @hasrole('super-admin')
                        <a href="{{ route('roles.edit', $role->id)}}" class="btn btn-warning">Edit</a>
                        <form action="{{ route('roles.destroy', $role->id)}}" id="form-delete{{$role->id}}" method="POST">
                            @csrf
                            @method('delete')
                            
                        </form>                   
                            <button class="btn btn btn-delete btn-danger" type="submit" data-id={{$role->id}}>Delete</button> 
                        @endhasrole
                                         
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $roles->links()}}
    </div>

</div>
    
@endsection
@section('script')   
@endsection