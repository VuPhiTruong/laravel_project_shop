@extends('admin.layouts.app')
@section('title','Users')
@section('content')

<div class="card">

    @if (session('message'))
        <h3 class="text-primary">
            {{session('message')}}
        </h3>
    @endif

    <h1>
        User list
    </h1>
        @can('create-user')
            <div>
                <a href="{{ route('users.create')}}" class="btn btn-primary">Create</a>
            </div>
        @endcan
        
    <div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
            @foreach ($users as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>
                        <img src="{{ $item->images->first() ? asset('upload/'.$item->images->first()->url) : asset('upload/default.png') }}"
                             width="200px" height="200px" alt="">
                    </td>
                    
                    <td>{{$item->name}}</td>
                    <th>{{$item->email}}</th>
                    <td>{{$item->phone}}</td>
                    <td>
                        @can('update-user')
                            <a href="{{ route('users.edit', $item->id)}}" class="btn btn-warning">Edit</a>
                        @endcan
                        @can('delete-user')
                            <form action="{{ route('users.destroy', $item->id)}}" id="form-delete{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            
                            </form>                   
                                <button class="btn btn btn-delete btn-danger"  type="submit"  data-id={{ $item->id }}>Delete</button> 
                        @endcan
                                          
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $users->links()}}
    </div>

</div>
    
@endsection
@section('script')   
@endsection