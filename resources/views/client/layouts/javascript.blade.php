<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('client/lib/easing/easing.min.js')}}"></script>
    <script src="{{asset('client/lib/owlcarousel/owl.carousel.min.js')}}"></script>

    <!-- Contact Javascript File -->
    <script src="{{asset('client/mail/jqBootstrapValidation.min.js')}}"></script>
    

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src={{asset('lodash/lodash.js')}}></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Template Javascript -->
    <script src="{{asset('admin/assets/base/base.js')}}"></script>