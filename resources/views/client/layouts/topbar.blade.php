<div class="container-fluid">
    <div class="row bg-secondary py-2 px-xl-5">
        <div class="col-lg-6 d-none d-lg-block">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark" href="">FAQs</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Help</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Support</a>
            </div>
        </div>
        <div class="col-lg-6 text-center text-lg-right">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark px-2" href="">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-twitter"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a class="text-dark pl-2" href="">
                    <i class="fab fa-youtube"></i>
                </a>
                
            </div>
        </div>
    </div>
    <div class="row align-items-center py-3 px-xl-5">
        <div class="col-lg-3 d-none d-lg-block">
            <a href="{{ route('client.home')}}" class="text-decoration-none">
                <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">Mike</span>Shop</h1>
            </a>
        </div>

        <div class="col-lg-6 col-6 text-left">
            @if(!isset($hideshearch) || !$hideshearch)
                <form action="{{ route('client.home.search') }}" method="GET">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search product by name" name="name">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            @endif
            
        </div>
        <div class="col-lg-3 col-6 text-right">
            
            <a href="{{route('client.cart.index')}}" class="btn border">
                <i class="fas fa-shopping-cart text-primary" style="margin-top: 0;"></i>
                <span class="badge" id="productCountCart">{{ $countProductInCart }}</span>
            </a>
            <a class="text-dark px-2" href="{{route('client.home.account')}}" style="font-size: 24px;">
                <i class="fas fa-user"></i>
            </a>
        </div>       
    </div>
</div>