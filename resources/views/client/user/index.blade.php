<!-- Featured Start -->
@extends('client.layouts.app')
@section('title','Profile')
@section('content')
@php
    $hideNavbar = true;
@endphp
@php
    $hideshearch = true;
@endphp

<div class="row justify-content-end align-items-center">
    @if (session('message'))
        <h3 style="color: red;">
            {{session('message')}}
        </h3>
    @endif
    <div class="col-auto">
        @if(auth()->check())                                
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>                                
        @else           
            <a href="{{route('login')}}" class="btn btn-primary">Login</a>
            <a href="{{route('register')}}" class="btn btn-primary">Register</a>
        @endif
    </div>
</div>
<div class="container">
    <div class="card user-card">
        <h1 class="card-header">Profile</h1>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">                 
                    <div class="user-image-container">
                        <img src="{{ $user->images ? asset('upload/'.$user->images->first()->url) : 'upload/default.jpg'}}" id="show-image" alt="" class="user-image">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="mb-3">
                        <label><strong>Name: </strong></label>
                        <span>{{ $user->name }}</span>
                    </div>
                    <div class="mb-3">
                        <label><strong>Email: </strong></label>
                        <span>{{ $user->email }}</span>
                    </div>
                    <div class="mb-3">
                        <label><strong>Phone: </strong></label>
                        <span>{{ $user->phone }}</span>
                    </div>
                    <div class="mb-3">
                        <label><strong>Address: </strong></label>
                        <span>{{ $user->address }}</span>
                    </div>
                    <div class="mb-3">
                        <label><strong>Gender: </strong></label>
                        <span>{{ $user->gender }}</span>
                    </div>
                    <div class="mb-3">
                        <label><strong>Role: </strong></label>
                        @foreach ($roles as $role)    
                            <span>{{ $role->name }} ,</span>               
                        @endforeach                       
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
    <div class="row justify-content-end align-items-center">
        <div class="col-auto ml-auto mt-2">
            <a href="{{ route('client.view.profile.update') }}" class="btn btn-primary">Update</a>
        </div>
    </div>
    
</div>

@endsection