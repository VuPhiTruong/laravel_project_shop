@extends('client.layouts.app')
@section('title','Edit Profile')
@section('content')
@php
    $hideNavbar = true;
@endphp
@php
    $hideshearch = true;
@endphp

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="col-md-8">
            <div class="user-card">
                    <h1 class="card-header">Edit Profile</h1>
                <div class="card-body">
                    <form action="{{ route('client.profile.update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <div class="mb-3">
                            <div class="input-group-static col-5 mb-4">
                                <label for="">Image</label>
                                <input type="file" accept="image/*" name="image" id="image-input" class="form-control">                           
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-5">
                                <img src="{{ $user->images->first() ? asset('upload/'.$user->images->first()->url) : asset('upload/default.png') }}"
                                    id="show-image" alt="" width="300px">
        
                            </div>                           
                        </div>
                        

                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" value="{{ old('name') ?? $user->name }}" name="name" id="name" class="form-control">
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" value="{{ old('email') ?? $user->email }}" name="email" id="email" class="form-control">
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="text" value="{{ old('phone') ?? $user->phone }}" name="phone" id="phone" class="form-control">
                            @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="gender" class="form-label">Gender</label>
                            <select name="gender" id="gender" class="form-control">
                                <option value="male" {{ $user->gender == 'male' ? 'selected' : '' }}>Male</option>
                                <option value="female" {{ $user->gender == 'female' ? 'selected' : '' }}>Female</option>
                            </select>
                            @error('gender')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="address" class="form-label">Address</label>
                            <textarea name="address" id="address" class="form-control">{{ old('address') ?? $user->address }}</textarea>
                            @error('address')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mb-3 text-center">
                            <button type="submit" class="btn btn-primary">a</button>
                            {{-- <a href="{{ route('client.profile.update') }}" class="btn btn-primary">Update</a> --}}
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')   
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" 
        crossorigin="anonymous">
    </script>
    <script>
        $(() => {   
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#show-image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('#image-input').change(function() {
                readURL(this);
            });
        });
    </script>
@endsection
